# Template information

This is a GitLab project template.
For a overview of all available project templates see https://gitlab.com/gitlab-org/project-templates/

## Update to new major version

To get ready for a new TYPO3 major release,
it is necessary to update all package names
starting with `typo3/cms-`

Step by step:

1. Set new version in `composer.json` for all `typo3/cms-*`
2. Update `platform:php` `composer.json` according to the requirements
3. Update `php_version` in `.ddev/config.yaml` according to the requirements

## Update project template in GitLab

Usually done by GitLab staff

Setup GDK according to the [docs](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#one-line-installation)
Ensure GDK is up running as expected (https://127.0.0.1:3000/)

```
gdk start
```

To export and download the repository as tar.gz and store it in `gitlab-development-kit/gitlab/vendor/project_templates/typo3_distribution.tar.gz`
run the following command:
```
bundle exec rake gitlab:update_project_templates\[ochorocho/typo3-distribution\]
```

See [details](https://docs.gitlab.com/ee/development/project_templates.html#test-your-built-in-project-with-the-gitlab-development-kit)

## Why using the GitLab Runner

It seems a good option for the future. We simply
utilize the runner to run single jobs locally.

On top, we can hopefully choose between podman and docker.
This is just an idea. Nothing is carved in stone so far.
